package regexapplauncher;

import org.apache.commons.cli.Options;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.HelpFormatter;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

/** Class RegExAppLauncher
 * This is the main function for RegExApp.
 * RegExAppLauncher will either process the URI passed to it against a list of regular expressions,
 * or will open the configuration dialog.
 * @author mlinton
 *
 */

public class RegExAppLauncher {
	private static String str_appname = "RegExAppLauncher";
	private static String str_appversion = "0.1";
		
	public static void main(String[] args) {
		String file_appconfig = null;
		String file_usrconfig = null;
		String file_regexdata = null;
		
		String str_tgturi = "";
		
		Boolean isconfig = false;
		Boolean isverbose = false;
		
		Options opt_clopts = new Options();
		CommandLineParser clp_clparser = new DefaultParser();
		CommandLine cl_cmd = null;
		
		// load default config
		
		// load user config
		
		// Configure command line parsing
		// TODO use buildconfig for better options
		opt_clopts.addOption("c", "config", false, "Open the configuration dialog");
		opt_clopts.addOption("d", "datafile", true, "Use this data file that contains RegEx rules");
		opt_clopts.addOption("h", "help", false, "This help screen");
		opt_clopts.addOption("v", "verbose", false, "Enable verbose output");

		// Parse command line options
		try {
			cl_cmd = clp_clparser.parse(opt_clopts, args);
			
			// Help or version information
			if (cl_cmd.hasOption("help")) {
				printHelp(opt_clopts);
				System.exit(0);
			}
			
			// Commandline flag processing
			if (cl_cmd.hasOption("config")) { isconfig = true; } 			
			
			if (cl_cmd.hasOption("datafile")) {
				file_regexdata = cl_cmd.getOptionValue("datafile");
			}
			
			if (cl_cmd.hasOption("verbose")) { isverbose = true; }
			
		} catch (ParseException e) {
			System.exit(1);
		}

		if (isconfig) {
			// launch the configuration dialog
			Display display = new Display();
			Shell shell = new Shell(display);
			shell.setText(str_appname + " Configuration");
			shell.setSize (392, 318);
			
			Composite cmp_reac = new RegExAppConfig(shell, SWT.NONE, str_appname + " v" + str_appversion);
			cmp_reac.setLocation(0, 0);
			cmp_reac.setSize(378, 275);
			cmp_reac.setLayout(null);
			
			shell.open ();
			while (!shell.isDisposed ()) {
				if (!display.readAndDispatch ()) display.sleep ();
			}
			display.dispose ();

		} else {
			// get remaining options passed without flags this should just be the URI
			if (cl_cmd.getArgList().size() < 1) {
				System.out.println("Too few arguments");
				System.exit(1);
			} else if (cl_cmd.getArgList().size() > 1) {
				System.out.println("Too many arguments");
				System.exit(1);
			} else {
				str_tgturi = cl_cmd.getArgList().get(0);
			}
			
			// Loop through the regex config file and build the regex list (pass to mainapp)
			RegExApp rea_mainapp = new RegExApp();
			parseRegExXMLFile(rea_mainapp, "C:\\Users\\USHU45112\\AppData\\Roaming\\RegExApp\\regexlist_win.xml", str_tgturi);
			
			// process the regex
			RegExMap rem_cmd = rea_mainapp.get_RunCommand();
			
			if (rem_cmd.app == "") {
				System.out.println("No matches found");
			} else {
				// Launch the URI with the Program
				Runtime run_program = Runtime.getRuntime();
				try {
		            run_program.exec(rem_cmd.app + " " + rem_cmd.uri);
		        } catch (IOException e) {
		        	System.err.println("Program \"" + rem_cmd.app + "\" not found");
		            if (isverbose) { e.printStackTrace(); }
		        }
			}
			
		} // END isconfig
		
	} // END main
	
	private static void printHelp(Options clopts) {
		HelpFormatter hf_help = new HelpFormatter();
		
		// Print version information
		System.out.println(str_appname + " v" + str_appversion);
		
		// Print Help
		hf_help.printHelp(str_appname + " [opts] <URI>", clopts);
	}// END PrintHelp
	
	private static void parseRegExXMLFile (RegExApp rea_mainapp, String str_regexlist, String str_tgturi) {
		RegExMap arr_map = new RegExMap();
		arr_map.uri = str_tgturi;
		
		Boolean b_regex = false;
		Boolean b_launcher = false;
		
		try {
			XMLInputFactory xif_factory = XMLInputFactory.newInstance();
			XMLEventReader xer_eventreader = xif_factory.createXMLEventReader(new FileReader(str_regexlist));
			
			while(xer_eventreader.hasNext()) {
				XMLEvent xe_event = xer_eventreader.nextEvent();
				       
				switch(xe_event.getEventType()) {
				       
					case XMLStreamConstants.START_ELEMENT:
						StartElement startElement = xe_event.asStartElement();
						String str_elementname = startElement.getName().getLocalPart();
						
						if (str_elementname.equalsIgnoreCase("regex")) {
							b_regex = true;
						} else if (str_elementname.equalsIgnoreCase("launcher")) {
							b_launcher = true;
						}
					break;
					
					case XMLStreamConstants.CHARACTERS:
						Characters char_streamchar = xe_event.asCharacters();
						if(b_regex) {
							arr_map.regex = char_streamchar.getData();
							b_regex = false;
						} else if (b_launcher) {
							arr_map.app = char_streamchar.getData();
							b_launcher = false;
						}
					break;
					
					case XMLStreamConstants.END_ELEMENT:
						EndElement ee_element = xe_event.asEndElement();
						
						if(ee_element.getName().getLocalPart().equalsIgnoreCase("expression")) {
							rea_mainapp.add_RegExMap(arr_map);
							//arr_map.regex = "";
							//arr_map.app = "";
						}
					break;
				} 
			}
			} catch (FileNotFoundException e) {
				System.err.println("Unable to find File \"" + str_regexlist + "\"");
			} catch (XMLStreamException e) {
				e.printStackTrace();
		}
	} // END ParseRegExXMLFile

} // END RegExAppLauncher
